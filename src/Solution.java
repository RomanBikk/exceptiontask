import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        try {
            SomeClass.exceptionThrower();

        } catch (ExceptionClass exceptionClass) {
            exceptionClass.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите числа.");
        try {
            int input1 = Integer.parseInt(reader.readLine());
            int input2 = Integer.parseInt(reader.readLine());
            reader.close();
            try {
                int result = IDivsion.division(input1, input2);
                System.out.println(result);
            } catch (ArithmeticException e) {
                e.printStackTrace();
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }
}
